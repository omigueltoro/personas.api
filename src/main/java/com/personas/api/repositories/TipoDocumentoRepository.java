/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.personas.api.repositories;
import com.personas.api.commons.IGenericRepository;
import com.personas.api.models.TipoDocumento;
import org.springframework.stereotype.Repository;
/**
 *
 * @author otoro
 */
@Repository 
public interface TipoDocumentoRepository extends IGenericRepository <TipoDocumento> {

}
