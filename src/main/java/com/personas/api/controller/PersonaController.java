package com.personas.api.controller;

import java.util.Comparator;
import java.util.List;
import com.personas.api.commons.IGenericRepository;
import com.personas.api.models.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
@RequestMapping(value= "/api/v1/")
public class PersonaController {
	
	@Autowired
	private IGenericRepository<Persona> repository; 
	
	@GetMapping(value="/personas")
	public List<Persona> getAll(){
		Comparator<Persona> comparator = new Comparator<Persona>() {
			@Override
			public int compare(Persona left, Persona right) {
				return left.getId() - right.getId();
			}
		};
		List<Persona> list = repository.findAll();
		list.sort(comparator);

		return list;
	}
	
	@GetMapping(value="/personas/{id}")
	public Persona find(@PathVariable Integer id) {
		return repository.get(id);
	}

	@PostMapping(value = "/personas")
	public ResponseEntity<Persona> save(@RequestBody Persona persona) {

		Persona obj = repository.save(persona);
		return new ResponseEntity<Persona>(obj, HttpStatus.OK);

	}

	@DeleteMapping(value = "/personas/{id}")
	public ResponseEntity<Persona> delete(@PathVariable Integer id) {

		Persona persona = repository.get(id);
		if(persona != null) {
			repository.delete(persona);
		}else {
			return new ResponseEntity<Persona>(persona, HttpStatus.NOT_FOUND);	 
		}
		return new ResponseEntity<Persona>(persona, HttpStatus.OK);	
		
	}

}
