package com.personas.api.controller;

import java.util.Comparator;
import java.util.List;
import com.personas.api.commons.IGenericRepository;
import com.personas.api.models.Pais;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
@RequestMapping(value= "/api/v1/")
public class PaisController {
	
	@Autowired
	private IGenericRepository<Pais> repository;
	
	@GetMapping(value="/paises")
	public List<Pais> getAll(){
		Comparator<Pais> comparator = new Comparator<Pais>() {
			@Override
			public int compare(Pais left, Pais right) {
				return left.getId() - right.getId();
			}
		};
		List<Pais> list = repository.findAll();
		list.sort(comparator);

		return list;
	}
	
	@GetMapping(value="/paises/{id}")
	public Pais find(@PathVariable Integer id) {

		return repository.get(id);
	}
	@PostMapping(value="/paises")
	public ResponseEntity<Pais> save(@RequestBody Pais pais){
		
		Pais obj = repository.save(pais);
		return new ResponseEntity<Pais>(obj, HttpStatus.OK);		
	}	
	
	@DeleteMapping(value="/paises/{id}")
	public ResponseEntity<Pais> delete(@PathVariable Integer id){
		
		Pais pais = repository.get(id);
		if(pais != null) {
			repository.delete(pais);
		}else {
			return new ResponseEntity<Pais>(pais, HttpStatus.NOT_FOUND);	 
		}
		return new ResponseEntity<Pais>(pais, HttpStatus.OK);
	}

}
