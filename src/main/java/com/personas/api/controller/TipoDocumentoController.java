package com.personas.api.controller;

import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.personas.api.commons.IGenericRepository;
import com.personas.api.models.TipoDocumento;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
@RequestMapping(value= "/api/v1/")
public class TipoDocumentoController {
	
	@Autowired
	private IGenericRepository<TipoDocumento> repository;
	
	@GetMapping(value="/tipos-documento")
	public List<TipoDocumento> getAll(){
		Comparator<TipoDocumento> comparator = new Comparator<TipoDocumento>() {
			@Override
			public int compare(TipoDocumento left, TipoDocumento right) {
				return left.getId() - right.getId();
			}
		};
		List<TipoDocumento> list = repository.findAll();
		list.sort(comparator);

		return list;
	}
	
	@GetMapping(value="/tipos-documento/{id}")
	public TipoDocumento find(@PathVariable final Integer id) {
		return repository.get(id);
	}
	@PostMapping(value="/tipos-documento")
	public ResponseEntity<TipoDocumento> save(@RequestBody final TipoDocumento tipoDocumento){
		
		final TipoDocumento obj = repository.save(tipoDocumento);
		return new ResponseEntity<TipoDocumento>(obj, HttpStatus.OK);		
		
	}	
	
	@DeleteMapping(value="/tipos-documento/{id}")
	public ResponseEntity<TipoDocumento> delete(@PathVariable final Integer id){
		
		final TipoDocumento tipoDocumento = repository.get(id);
		if(tipoDocumento != null) {
			repository.delete(tipoDocumento);
		}else {
			return new ResponseEntity<TipoDocumento>(tipoDocumento, HttpStatus.NOT_FOUND);	 
		}
		return new ResponseEntity<TipoDocumento>(tipoDocumento, HttpStatus.OK);	
		
	}

}
