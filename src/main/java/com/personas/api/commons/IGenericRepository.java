/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.personas.api.commons;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
/**
 *
 * @author otoro
 */
public interface IGenericRepository <T> extends CrudRepository <T,Integer> {

    @Override
    List <T> findAll();

	@Override
    List <T> findAllById(Iterable<Integer> ids);
    
    default T get (Integer id) {
        Optional<T> object  = findById(id);

        if(object.isPresent()){
            return object.get();
        }

		return null;
    }
}
